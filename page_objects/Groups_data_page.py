import json
import time
from selenium.webdriver.common.action_chains import ActionChains

class Test_Groups_page():

    group_tab = "(//a[@class='nav-link'])[3]"
    first_group = "(//span[@class='btn-link'])[1]"
    first_form = "(//*[@class='btn-link'])[1]"
    page_navigation = "(//*[@class='btn btn-info btn-sm'])[2]"
    actions_drop_down = "//*[@id='actionsMenuButton']"
    view_overall_results = "(//*[@class='dropdown-item'])[5]"
    list_of_all_values = "//*[@class='form-detail-item']"
    view_decision = "//*[text()='View Decision ']"
    file_path = "/Users/joeyfrmfrnds/Documents/test_project/data/data_names.json"

    def __init__(self, driver):
        self.driver = driver

    def click_a_groups_tab(self):
        time.sleep(10)
        self.driver.find_element_by_xpath(self.group_tab).click()


    def click_b_first_group(self):
        time.sleep(4)
        self.driver.find_element_by_xpath(self.first_group).click()
        time.sleep(4)


    def click_c_first_form(self):
        time.sleep(4)
        self.driver.find_element_by_xpath(self.first_form).click()
        time.sleep(8)

    def click_d_actions_drop_down(self):
        time.sleep(4)
        self.driver.find_element_by_xpath(self.page_navigation).click()
        time.sleep(10)
        self.driver.find_element_by_xpath(self.actions_drop_down).click()
        time.sleep(2)


    def click_e_view_overall_results(self):
        time.sleep(2)
        self.driver.find_element_by_xpath(self.view_overall_results).click()
        time.sleep(3)

    def get_f_list_of_form_values(self):
        time.sleep(3)
        GT_values = self.driver.find_elements_by_xpath(self.list_of_all_values)
        time.sleep(5)
        file1 = open("/Users/joeyfrmfrnds/PycharmProjects/Omni_Test_Automation/data/data_names.json", 'r')
        json_input = file1.read()
        inputBody = json.loads(json_input)
        json_data = list(inputBody.values())
        print("Json_data:")
        # print(json_data)
        data_list1 = []
        for data in GT_values:
            data_list = data.text
            data_list1.append(data_list)
        data_list1.pop(0)
        print("Data List1:")
        print(data_list)
        found_data = []
        for i in range(len(json_data)):
            for j in range(len(data_list1)):
                if data_list1[j] == json_data[i]:
                    data_list1[j] = 0
                    json_data[i] = 0

        for i in range(len(json_data)):
            if json_data[i] != 0:
                print("Expected Data: " + json_data[i])

        print("------------------------------------------------------")
        for i in range(len(data_list1)):
            if data_list1[i] != 0:
                print("Actual Data: " + data_list1[i])

    def click_g_view_decision_form(self):
        time.sleep(2)
        self.driver.implicitly_wait(100)
        self.driver.find_element_by_xpath(self.view_decision).click()