import unittest
import HtmlTestRunner
from selenium import webdriver
import os
import sys
from Android_directory.Android_test import Automation_android, driver


class Android_Runner_class(unittest.TestCase):
    auto_android = Automation_android(driver)

    @classmethod
    def setUpClass(cls):
        print("test starts")

    def test_a_desired_capabilities(self):

        self.auto_android.set_desired_capabilities()

    def test_b_capture_image_start__new_assessment(self):

        self.auto_android.click_menu_tab()
        self.auto_android.click_home_button()
        self.auto_android.click_start_new_assessment()
        self.auto_android.click_capture_button()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_c_select_image_start_new_assessment(self):

        self.auto_android.click_start_new_assessment()
        self.auto_android.click_and_select_image()
        self.auto_android.click_select_image_ok()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_d_select_multiple_images_start_new_assessment(self):

        self.auto_android.click_start_new_assessment()
        self.auto_android.click_and_select_image()
        self.auto_android.click_and_select_second_image()
        self.auto_android.click_select_image_ok()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_e_start_new_assessment_delete_one_image(self):

        self.auto_android.click_start_new_assessment()
        self.auto_android.click_and_select_image()
        self.auto_android.click_and_select_second_image()
        self.auto_android.click_select_image_ok()
        self.auto_android.select_a_form()
        self.auto_android.delete_a_form()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_f_start_new_assessment_delete_all_image(self):

        self.auto_android.click_start_new_assessment()
        self.auto_android.click_and_select_image()
        self.auto_android.click_and_select_second_image()
        self.auto_android.click_select_image_ok()
        self.auto_android.click_select_button()
        self.auto_android.click_select_all_button()
        self.auto_android.delete_all_forms()

    def test_g_standard_capture_by_camera(self):

        self.auto_android.click_camera_icon()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_h_standard_capture_multiple_images_by_camera(self):

        self.auto_android.click_camera_icon()
        self.auto_android.click_capture_button()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_i_delete_a_image_by_standard_camera(self):

        self.auto_android.click_camera_icon()
        self.auto_android.click_capture_button()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.select_a_form()
        self.auto_android.delete_a_form()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_j_delete_all_images_by_standard_camera(self):

        self.auto_android.click_camera_icon()
        self.auto_android.click_capture_button()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.click_select_button()
        self.auto_android.click_select_all_button()
        self.auto_android.delete_all_forms()

    def test_k_ar_capture_by_camera(self):

        self.auto_android.click_camera_icon()
        self.auto_android.click_ar()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_l_ar_capture_multiple_images_by_camera(self):

        self.auto_android.click_camera_icon()
        self.auto_android.click_ar()
        self.auto_android.click_ar()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_m_delete_an_image_by_ar_camera(self):

        self.auto_android.click_camera_icon()
        self.auto_android.click_ar()
        self.auto_android.click_ar()
        self.auto_android.click_next_button()
        self.auto_android.select_a_form()
        self.auto_android.delete_a_form()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_n_delete_all_images_by_ar_camera(self):

        self.auto_android.click_camera_icon()
        self.auto_android.click_ar()
        self.auto_android.click_ar()
        self.auto_android.click_next_button()
        self.auto_android.click_select_button()
        self.auto_android.click_select_all_button()
        self.auto_android.delete_all_forms()

    def test_o_recapture_image_standard_camera(self):
        self.auto_android.click_camera_icon()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.click_select_button()
        self.auto_android.click_select_all_button()
        self.auto_android.click_recapture_button()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_p_recapture_image_ar_camera(self):
        self.auto_android.click_camera_icon()
        self.auto_android.click_ar()
        self.auto_android.click_next_button()
        self.auto_android.click_select_button()
        self.auto_android.click_select_all_button()
        self.auto_android.click_recapture_button()
        self.auto_android.click_ar()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_q_recapture_image_start_new_assessment(self):
        self.auto_android.click_start_new_assessment()
        self.auto_android.click_and_select_image()
        self.auto_android.click_select_image_ok()
        self.auto_android.click_select_button()
        self.auto_android.click_select_all_button()
        self.auto_android.click_recapture_button()
        self.auto_android.click_capture_button()
        self.auto_android.click_next_button()
        self.auto_android.click_send_document()
        self.auto_android.get_results_text()

    def test_r_delete_old_files_gallery_images(self):
        self.auto_android.click_menu_tab()
        self.auto_android.click_delete_old_files_tab()
        self.auto_android.select_image_1_gallery_images()
        self.auto_android.select_image_2_gallery_images()
        self.auto_android.click_delete_button_gallery_images()
        self.auto_android.click_back_button_gallery_images()


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(
        output='D:/Automation/Omni_Test_Automation/reports'))
